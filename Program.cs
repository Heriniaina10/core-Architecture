﻿using System;
using console_andrana.Models;
using console_andrana.Persistence;
using console_andrana.Persistence.Unit;

namespace console_andrana
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("---------début---------");

            using(var unitOfWork = new UnitOfWork(new AppDbContext())) {
                unitOfWork.CouresRepository.Add(new Course { Name = "andrana" });
                unitOfWork.Complete();
            }

            Console.WriteLine("---------fin---------");
            Console.ReadLine();
        }
    }
}
