using console_andrana.Models;
using Microsoft.EntityFrameworkCore;

namespace console_andrana.Persistence
{
    public class AppDbContext : DbContext
    {
        // public AppDbContext(DbContextOptions<AppDbContext> options)
        //     : base(options)
        // { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=(LocalDb)\\MSSQLLocalDB;Initial Catalog=c-andrana;integrated security=True;MultipleActiveResultSets=true");
        }


    /*
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // 01 Model ==> n Vehicles
            // modelBuilder.Entity<Vehicle>()
            //     .HasOne(x => x.Model)
            //     .WithMany(y => y.Vehicles)
            //     .HasForeignKey(x => x.ModelId);

            // 01 Make ==> n Model
            modelBuilder.Entity<Model>()
                .HasOne(x => x.Make)
                .WithMany(y => y.Models)
                .HasForeignKey(x => x.MakeId);

            // n Features ==> n Vehicles
            modelBuilder.Entity<VehicleFeatures>()
                .HasKey(vf => new {
                    vf.VehicleId,
                    vf.FeatureId
                });
        }
    */

        public DbSet<Course> Courses { get; set; }
    }
}