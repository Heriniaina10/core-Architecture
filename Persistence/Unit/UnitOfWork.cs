using console_andrana.Core.Repository.Courses;
using Microsoft.EntityFrameworkCore;

namespace console_andrana.Persistence.Unit
{
    public class UnitOfWork : IUnitOfWork
        // where TContext : DbContext
    {
        private readonly AppDbContext _context;

        public UnitOfWork(AppDbContext context)
        {
            _context = context;
            CouresRepository = new CourseRepository(_context);
        }

        public ICourseRepository CouresRepository { get; private set; }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}