using System;
using console_andrana.Core.Repository.Courses;

namespace console_andrana.Persistence.Unit
{
    public interface IUnitOfWork : IDisposable
    {
        ICourseRepository CouresRepository { get; }
        int Complete();
    }
}