using System.Collections.Generic;
using console_andrana.Models;

namespace console_andrana.Core.Repository.Courses
{
    public interface ICourseRepository : IRepository<Course>
    {
        IEnumerable<Course> GetTopSellingCourses(int count);
    }
}