using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using console_andrana.Models;
using console_andrana.Persistence;
using Microsoft.EntityFrameworkCore;

namespace console_andrana.Core.Repository.Courses
{
    public class CourseRepository : Repository<Course, AppDbContext>, ICourseRepository
    {
        public AppDbContext CurrentContext => Context as AppDbContext;

        public CourseRepository(AppDbContext context) : base(context)
        { }

        public IEnumerable<Course> GetTopSellingCourses(int count)
        {
            return CurrentContext.Courses.OrderByDescending(c => c.Name)
                .Take(count)
                .ToList();
        }
    }
}