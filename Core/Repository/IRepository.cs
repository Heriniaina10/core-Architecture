using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace console_andrana.Core.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity Get (int id);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);

        void Add(params TEntity[] entity);
        void Remove(params TEntity[] entity);
    }
}