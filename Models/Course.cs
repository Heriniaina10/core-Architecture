using System.ComponentModel.DataAnnotations;

namespace console_andrana.Models
{
    public class Course
    {
        public int Id { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
    }
}